#!/usr/bin/env python

import argparse, socket, os
import twisted
from time import sleep
from twisted.internet import reactor, protocol
from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.static import File
from twisted.web.twcgi import CGIDirectory


class AAWebSocketProtocol(WebSocketServerProtocol):
    def __init__(self):
        # WebSocketServerProtocol.__init__(self) # apparently there isn't one
        self.process = None
 
    def onMessage(self, msg, binary):
        if (self.process == None):
            self.process = AAProcessProtocol(self)
            parts = msg.strip().split()
            reactor.spawnProcess(self.process, parts[0], parts, {})
            # self.sendMessage("Starting {0}...\n", False)

    def onClose (self, wasclean, code, reason):
        # web socket has closed, kill the process
        print "web socket onClose", wasclean, code, reason
        if (self.process != None):
            self.process.kill()
            self.process = None

    def ended (self, code):
        # process has ended (normally), close ourself
        if (self.process != None):
            # close outself
            self.transport.loseConnection()

class AAProcessProtocol(protocol.ProcessProtocol):

    def __init__(self, ws):
        # self.output = ""
        self.ws = ws
        self.bytesReceived = 0

    def connectionMade(self):
        print "connectionMade"
        self.transport.closeStdin() # no input to mplayer

    def outReceived(self, data):
        print "outReceived, with %d bytes!" % len(data)
        # self.output += data
        # print data
        self.bytesReceived += len(data)
        print "{0} bytes received".format(self.bytesReceived)
        self.ws.sendMessage(data, isBinary=True, doNotCompress=True)

    def errReceived(self, data):
        print "errReceived, with %d bytes!" % len(data)
        print "<ERR>", data,

    def inConnectionLost(self):
        print "inConnectionLost, stdin is closed! (we probably did it)"

    def outConnectionLost(self):
        print "outConnectionLost! The child closed their stdout!"
        # evt here code to (trigger) a processing of the output

    def errConnectionLost(self):
        print "errConnectionLost! The child closed their stderr."

    def processExited(self, reason):
        print "processExited, status %d" % (reason.value.exitCode,)

    def processEnded(self, reason):
        print "processEnded, status %d" % (reason.value.exitCode,)
        self.ws.ended(reason.value.exitCode)

    def kill (self):
        # NOTES
        # Here's the trace of a kill (when triggered by websocket.onClose)
        # web socket onClose True None None
        # errConnectionLost! The child closed their stderr.
        # outConnectionLost! The child closed their stdout!
        # processExited, status 1
        # processEnded, status 1

        # self.transport supports methods: write, closeStdin, closeStdout, closeStderr, loseConnection (close all 3)
        # kill seems to cause errors while loseConnection closes things gracefully
        # self.transport.signalProcess("KILL")

        self.transport.loseConnection()

def main (args):
    parser = argparse.ArgumentParser(description='Happy to serve you')
    parser.add_argument('--port', type=int, default=8000, help='http port number')
    parser.add_argument('--wsport', type=int, default=9000, help='websocket port number')
    parser.add_argument('-t', '--notryports', default=True, action="store_false", help='if a port is busy, automatically try other ones')
    parser.add_argument('--share', default=False, action="store_true", help='Run as server accessible via your local network')
    args = parser.parse_args(args)


    tryports = args.notryports
    port = args.port
    ipaddr = None

    if args.share:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("wikipedia.org",80))
        ipaddr = s.getsockname()[0]
        interface = ipaddr # None would listen to all
        s.close()
    else:
        interface = "localhost"

    # HTTP
    resource = File(".") # Resource()
    # resource.putChild("f", File(cwd))
    # resource.putChild("lib", File(activearchives.getDataPath("htdocs")))
    # resource.putChild("cgi-bin", CGIDirectory(activearchives.getDataPath("cgi-bin")))
    # resource.putChild("", File(activearchives.getDataPath("root/index.html")))
    # resource.putChild("", File("."))
    factory = Site(resource)

    # WebSocket
    wsfactory = WebSocketServerFactory("ws://localhost:{0}".format(args.wsport), debug = False)
    wsfactory.protocol = AAWebSocketProtocol
    listenWS(wsfactory)
    # reactor.run()

    while True:
        try:
            if ipaddr:
                server_address = (ipaddr, port)
                servername = ipaddr
            else:
                server_address = ("", port)
                servername = "localhost"

            reactor.listenTCP(port, factory, interface=interface)
            print "(Twisted) Archiving starts now --> http://{0}:{1}".format(servername, port)
            reactor.run()

        except twisted.internet.error.CannotListenError:
            if tryports:
                if port < 2000:
                    port = 2000
                else:
                    port += 1
                sleep(.01)
            else:
                print """
====================================
Error: port ({0}) is already in use
====================================

You can pick another port number
(for example 9999) with:

    sosh --port 9999
""".format(port)
                break
        else:
            raise(e)

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])

 

