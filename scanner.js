$(document).ready(function () {
   function idler () {
      var idle_time = 0;

      function cancel() {
         idle_time = 0;
      }

      $(document).on("mousemove", function () {
         // console.log("move");
         cancel();
      });
   }
   idler();

   var state = 0,
      STATE_WAITING = 0,
      STATE_SCANNING = 1,
      STATE_PROCESSING = 2,
      STATE_UPLOADING = 3,
      STATE_POSTUPLOAD = 4;


   function refresh(newState) {
      state = newState;
      $(".process").removeClass("active");
      if (state == STATE_SCANNING) {
         $(".scanning").addClass("active")
      } else if (state == STATE_PROCESSING) {
         $(".processing").addClass("active");
      } else if (state == STATE_UPLOADING) {
         $(".uploading").addClass("active");
      } else if (state == STATE_POSTUPLOAD) {
         $(".done").addClass("active");
         // $("#waiting").show();
      }
   }

   function error (e) {
      refresh(STATE_WAITING);
      console.log("error", e);
   }

   function scan() {
      if (state == STATE_WAITING) {
         refresh(STATE_SCANNING);
         $("#waiting").show();
         $.ajax("/cgi-bin/scan.cgi", {
            dataType: "json", 
            success: function (result) {
               $("#preview").attr("src", result.output + "?tstamp="+(new Date().toISOString()) ).show();
               refresh(STATE_PROCESSING);
               $.ajax("/cgi-bin/process.cgi", {
                  data: {src: result.output},
                  dataType: "json",
                  success: function (result) {
                     $("#preview").attr("src", result.output + "?tstamp="+(new Date().toISOString())).show();
                     refresh(STATE_UPLOADING);
                     $.ajax("/cgi-bin/upload.cgi", {
                        data: {src: result.output},
                        dataType: "json",
                        success: function (result) {
                           refresh(STATE_POSTUPLOAD);
                           $("#waiting").hide();
                           window.setTimeout(function () {
                              refresh(STATE_WAITING);
                              $("#preview").attr("src", "").hide();
                           }, 4000);
                        },
                        error: error
                     });
                  },
                  error: error
               });
            },
            error: error
         });
      }
   }
   $("#scan").click(scan);

   $("#stop").click(function () {
      if (sock) {
         sock.close();
         sock = null;
      }
   })


});
