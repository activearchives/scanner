#!/usr/bin/env python

import cgitb; cgitb.enable()
import subprocess, json

output = subprocess.check_output("cgi-bin/scan.sh")
result = {}
result['output'] = output.strip()

print "Content-type: application/json; charset=utf-8"
print
print json.dumps(result)
