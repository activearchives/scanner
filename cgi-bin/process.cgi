#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import subprocess, json


fs = cgi.FieldStorage()
src = fs.getvalue("src")
output = subprocess.check_output('cgi-bin/process.sh "{0}"'.format(src), shell=True)
result = {}
result['output'] = output.strip()

print "Content-type: application/json; charset=utf-8"
print
print json.dumps(result)
