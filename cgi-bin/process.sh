#!/bin/bash

src=$1
p=${src%.*}.processed.png
convert "$src" -negate "$p"
echo $p

