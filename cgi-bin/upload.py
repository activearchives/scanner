import urllib2, sys, os
from poster.encode import multipart_encode, MultipartParam
from poster.streaminghttp import register_openers

register_openers()
for p in sys.argv[1:]:
    image_param = MultipartParam.from_file("thefile", p)
    datagen, headers = multipart_encode([image_param])
    request = urllib2.Request("http://sissv.activearchives.org/cgi-bin/jorn/upload.cgi", datagen, headers)
    print urllib2.urlopen(request).read()
