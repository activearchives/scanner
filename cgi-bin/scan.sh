#!/bin/bash

# scanimage --mode color --device genesys:libusb:001:005 > dump.pnm
f=scans/$(date -u +scan%Y%m%dT%H%MZ.pnm)
scanimage --mode color --device epson2 > $f
# scanimage --mode color > $f
png=${f%.*}.png
convert $f -resize 1024x $png
rm $f
echo $png
