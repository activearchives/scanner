#!/usr/bin/env python

import cgi, urllib2, json
from glob import glob
import cgitb; cgitb.enable()
from poster.encode import multipart_encode, MultipartParam
from poster.streaminghttp import register_openers


fs = cgi.FieldStorage()
src = fs.getvalue("src")

register_openers()
count = 0
uploads = glob("scans/{0}*".format(src))
uploads.append("scans/{0}.ok".format(src)) 
for f in uploads:
    image_param = MultipartParam.from_file("thefile", f)
    datagen, headers = multipart_encode([image_param])
    request = urllib2.Request("http://sissv.activearchives.org/cgi-bin/jorn/upload.cgi", datagen, headers)
    urllib2.urlopen(request).read()
    count += 1

print "Content-type: application/json; charset=utf-8"
print
print json.dumps({'output': src, 'count': count})

