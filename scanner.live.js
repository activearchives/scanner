$(document).ready(function () {

var ww = $(window).width(),
   wh = $(window).height(),
   canvas = $("<canvas></canvas>")
     .appendTo("#cc")
     .attr("width", ww)
     .attr("height", wh)
     .get(0),
   ctx = canvas.getContext("2d"),
   last_y;

function _sock (wsuri, opts) {
   // WebSocket Helper Function
   var that = { sock: new WebSocket(wsuri)};
   // http://stackoverflow.com/questions/13390454/receive-blob-in-websocket-and-render-as-image-in-canvas
   that.sock.binaryType = "arraybuffer";

   that.close = function () { that.sock.close(); }
   that.sock.onopen = function(e) { if (opts.open) { opts.open.call(that, e) } }
   that.sock.onmessage = function (e) {
      // console.log("sock.onmessage: ", e, e.data.byteLength, "bytes");
      // opts.data.call(that, e);
      var data = new Uint8Array(e.data);
      // console.log("data[0]", data[0]);
      opts.data.call(that, data);
   };
   that.sock.onclose = function(e) { if (opts.close) { opts.close.call(that, e) }; }
   return that;
}

function scan (opts) {
   var that = {},
      in_header = true,
      width,
      height,
      color = false,
      lines = [],
      curline = '',
      idata,
      idata_index,
      canvas,
      ctx,
      byte_pos = 0;

   that.check_bytes = 0;
   that.expected_bytes = 0;
   // create image data to hold the scan
   // draw this image data to a canvas ... to serve as an image source
   function init() {
      canvas = $("<canvas></canvas>").get(0);
      that.canvas = canvas;
      that.width = width;
      that.height = height;
      canvas.setAttribute("width", width);
      canvas.setAttribute("height", height);
      ctx = canvas.getContext("2d");
      idata = ctx.createImageData(width, height);
      that.idata = idata;
      that.expected_bytes = width*height*3;
      console.log("expected pixel bytes", that.expected_bytes);
   }

   that.data = function (curdata) {
      var data_index=0,
         len = curdata.length,
         parts;


      // console.log("received "+data.length+" bytes", ", total", check_bytes);
      // console.log("data", idata, data.length, data[0], idi);
      // return;
      if (in_header) {
         // console.log("Processing header");
         for (; data_index<len; data_index++) {
            // if (i < 10) { console.log(data[i]); }
            if (curdata[data_index] == 10) {
               // console.log("EOL", lines.length)
               lines.push(curline);
               curline = '';
               if (lines.length >= 4) {
                  header = false;
                  // console.log("end of headers", lines);
                  parts = lines[2].split(" ");
                  color = (lines[0] == "P6");
                  width = parseInt(parts[0]);
                  height = parseInt(parts[1]);
                  console.log("incoming scan:", width, "x", height, color ? "color" : "grayscale");
                  init(width, height);
                  idata_index = 0;
                  in_header = false;
                  console.log("post header, i is", data_index);
                  data_index += 1;
                  break;
               }
            } else {
               curline += String.fromCharCode(curdata[data_index]);
            }
         }
      }

      while (data_index < len) {
         idata.data[idata_index++] = curdata[data_index++];
         that.check_bytes++;
         if (++byte_pos == 3) {
            byte_pos = 0;
            idata.data[idata_index++] = 255;
         }
      }

      /*
      for (; data_index<len; data_index+=3) {
         //console.log(curdata[data_index]);
         //console.log(curdata[data_index+1]);
         //console.log(curdata[data_index+2]);

         idata.data[idata_index] = curdata[data_index];
         idata.data[idata_index+1] = curdata[data_index+1];
         idata.data[idata_index+2] = curdata[data_index+2];
         idata.data[idata_index+3] = 255;
         idata_index += 4;
         that.check_bytes += 3;
      }
      */
      // var cur_scan_position = { y: Math.floor(idata_index / (width*4)) };
      ctx.putImageData(idata, 0, 0);
      if (opts.draw) {
         opts.draw.call(that, idata_index);
      }
   }

   return that;
}

function start () {
   last_y = 0;
   var thescan = scan({
      draw: function (ii) {
         // // console.log("draw", this);
         // var cur_y = Math.floor(i / (this.width*4));
         // // ctx.drawImage(this.canvas, 0, -last_y, ww, wh);
         // // translate cur_y to screen y
         // ctx.drawImage(this.canvas, 0, last_y, this.width, (cur_y - last_y), 0, 0, ww, wh);
         // last_y = cur_y;
         var image = this;
         var cur_y = Math.floor(ii / (this.width*4));
         // ctx.drawImage(this.canvas, 0, -last_y, ww, wh);
         // translate cur_y to screen y
         var scale = (ww / this.width); // image to screen
         var source_height = wh / scale;
         var source_y = cur_y-source_height;
         var dest_y = 0;

         if (source_y < 0) {
            dest_y = -source_y * scale;
            source_y = 0;

         }

         // log("y", cur_y, source_height, source_height);
         // log(0, cur_y-(source_height/2), this.width);
         // log(0, 0, ww, wh);
         // ctx.drawImage(this.canvas, 0, 0, ww, wh);
         
         ctx.drawImage(this.canvas,
            0, source_y, this.width, source_height,
            0, dest_y, ww, wh);
         last_y = cur_y;
         
      }
   });
   _sock("ws://localhost:9000", {
      open: function (e) {
         //this.sock.send("./slowcat scan.50dpi.pnm");
         this.sock.send("scanimage --device epson2 --format pnm --mode color --resolution 50");
         // this.sock.send("cat scan.50dpi.pnm");
         // this.sock.send("./slowcat scan.50dpi.pnm");
         // this.sock.send("./slowcat scan.50dpi.pnm --sleeptime 50");
         // this.sock.send("./slowcat sample.4x4.pnm --sleeptime 50");
         console.log("SOCK.open")
      },
      data: function (data) {
         thescan.data(data); },
      close: function () {
         console.log("SOCK.close");
         console.log("checkbytes expected:", thescan.expected_bytes, "actual:", thescan.check_bytes );
      }
   });

}

$("#scan").click(start);

});
